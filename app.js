const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');  // https://www.npmjs.com/package/csurf
const flash = require('connect-flash');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const errorRoutes = require('./routes/error');
const errorController = require('./controllers/error');
const User = require('./models/user');

const MONGODB_URI =
  'mongodb://localhost:27017/node-mongoose';

const app = express();
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: 'sessions'
});
const csrfProtection = csrf();

app.set('view engine', 'ejs');
app.set('views', 'views');

//  extended=false is a configuration option that tells the parser to use the classic encoding.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// https://www.npmjs.com/package/express-session
app.use(session ({
    secret: 'my secret',
    resave: false, //Forces the session to be saved back to the session store, even if the session was never modified during the request.
    saveUninitialized: false, //Forces a session that is "uninitialized" to be saved to the store. A session is uninitialized when it is new but not modified.
    store: store
  })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then(user => {
      if (!user) {
        throw Error('Error retrieving user: ' + req.session.user._id);
      }
      req.user = user;
      next();
    })
    .catch(err => {
      throw Error(err);
    });
});

// An object that contains response local variables scoped to the request, and therefore available only 
// to the view(s) rendered during that request / response cycle (if any).
app.use((req, res, next) => {
  res.locals.isAuthenticated = req.session.isLoggedIn;
  res.locals.csrfToken = req.csrfToken();
  next();
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);
app.use(errorRoutes);

app.use(errorController.get404);

app.use((error, req, res, next) => {
  res.status(500)
    .render('error/500', {
      pageTitle: 'Error!',
      path: 'error/500',
      msg: error
    });
});

mongoose.connect(MONGODB_URI, {useNewUrlParser: true,  useUnifiedTopology: true})
  .then(result => {
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });
